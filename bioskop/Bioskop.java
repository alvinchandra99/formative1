class Bioskop{
    String name ="21";
    String address;
    String openingHours;

    public static void main(String[] args){
        Bioskop bioskop = new Bioskop();
        Studio studio = new Studio();
        Movies movies = new Movies();
        Employee employee = new Employee();
        Cashier cashier = new Cashier();
        Security security = new Security();
        
   
        System.out.println(bioskop.name);
        System.out.println(bioskop.address);

        System.out.println(studio.name);
        System.out.println(studio.seatsAmount);

        System.out.println(movies.price);
        System.out.println(movies.title);

        System.out.println(employee.name);
        System.out.println(employee.salary);

        System.out.println(cashier.hasilPenjualantTicket);

        System.out.println(security.certification);
        System.out.println(security.securityGears);
        
     


        
    }

}
class Studio extends Bioskop{
    String name = "A";
    int seatsAmount;

}

class Movies extends Bioskop{
    int price;
    String title;

}

class Employee extends Bioskop{
    String name;
    //Date
    int salary;
    int startWorkingHour;
    int endWorkingHour;
    String uniform;
    

}

class Cashier extends Employee{
    int hasilPenjualantTicket;


}

class Cleaning extends Employee{
    String[] cleaningGears;

}

class Security extends Employee{
    String[] certification;
    String[] securityGears;
}