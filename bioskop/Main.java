public class Main{
    public static void main(String[] args){
        Base b = new Derived();
        b.show();
    }
}

class Derived extends Base{
    public void show(){
        System.out.println("Derrived::show() called");
    }
}

class Base{
    public void show(){
        System.out.println("Base::show() called");
    }
}